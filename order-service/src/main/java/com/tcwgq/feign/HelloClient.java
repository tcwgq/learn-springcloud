package com.tcwgq.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "EUREKA-CLIENT", fallback = HelloClientHystrix.class)
public interface HelloClient {
    @GetMapping("/client")
    String client();
}
