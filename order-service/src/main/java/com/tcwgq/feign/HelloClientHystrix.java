package com.tcwgq.feign;

import org.springframework.stereotype.Component;

@Component
public class HelloClientHystrix implements HelloClient {
    @Override
    public String client() {
        return "This messange send failed !";
    }

}
