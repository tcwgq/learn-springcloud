package com.tcwgq.controller;

import com.tcwgq.feign.HelloClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class RibbonController {
    private static final String url = "http://EUREKA-CLIENT/client";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HelloClient helloClient;

    @GetMapping("/demo")
    public String demo() {
        return restTemplate.getForObject(url, String.class);
    }

    @GetMapping("/hello")
    public String hello() {
        return helloClient.client();
    }
}
