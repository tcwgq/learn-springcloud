package com.tcwgq.user.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @GetMapping("getName")
    public String getName(@RequestParam("name") String name) {
        System.out.println("name=" + name);
        return name;
    }

    @GetMapping("getAge")
    public Integer getAge(@RequestParam("age") Integer age) {
        System.out.println("age=" + age);
        return age;
    }
}
