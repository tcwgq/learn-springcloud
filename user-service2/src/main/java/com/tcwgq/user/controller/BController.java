package com.tcwgq.user.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/b")
public class BController {
    @RequestMapping("/getB")
    public String getA(){
       return "B";
    }
}